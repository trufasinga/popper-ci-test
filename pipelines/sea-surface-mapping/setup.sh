#!/bin/bash
# [wf] obtain and clean dataset
set -ex

# [wf] check if virtualenv is running

virtualenv /tmp/pipeline-env
source /tmp/pipeline-env/bin/activate

if [ "$VIRTUAL_ENV" != "" ]; then
    echo "You are in a working virtualenv $VIRTUAL_ENV";
    # Install required packages.
    pip install xarray
    pip install netCDF4
    pip install plotly
    pip install scipy
 else
    echo ERROR: virtualenv not found. Please make sure that virtualenv is installed and running.
    exit 1
fi
